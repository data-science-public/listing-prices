# Kavak Rental Listing Prices

Welcome to one of our take-home assignments.

## Setup
For running the environment, please click here:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-public%2Flisting-prices/HEAD)

## Scenario
The provided dataset contains attributes of AirBnB listings that are to be used to predict the price of the listing. The goal is to build a machine learning algorithm using the training dataset (`data/train.csv.zip`) that can achieve the least Root Mean Squared Error (RMSE) on the test dataset (test.csv.zip). This would be useful for suggesting prices to listing owners or identifying underpriced/overpriced listings.

## Deliverables
Please provide the following:
- Jupyter Notebook with code
- Predicted price for the listings in the test dataset (this will be compared to the true price which is not provided)
