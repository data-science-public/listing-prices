class Challenge:
    """Class for handling all remote files and functions.

    Methods
    -------
    load_data
    score
    """
    @classmethod
    def load_data(cls) -> list:
        """Downloads the necessary data.

        Returns
        -------
        list
            A list of three pandas.DataFrames: train, test and sample_submission.
        """
        import pandas as pd
        
        data = []
        for file in ['train', 'test', 'sample_submission']:
            data.append(
                pd.read_csv(f's3://airbnb-kavak/{file}.csv')
            )
        return data
    
    @classmethod
    def score(cls, df) -> float:
        """Returns RMSE for the test dataset.

        Parameters
        ----------
        df : pandas.DataFrame
            A DataFrame containing columns {'id', 'log_price'}.

        Returns
        -------
        float
            The RMSE of the submission.
        """
        import requests
        from io import BytesIO
        s = BytesIO()
        df.to_csv(s, index=False)
        response = requests.post(
            "https://f6oz4sctgzpjwlklebkxidumem0nhzez.lambda-url.us-west-2.on.aws/",
            data=s.getvalue()
        )
        return float(response.content)


if __name__ == "__main__":
    import pickle

    with open('challenge.pkl', 'wb') as f:
        pickle.dump(Challenge, f)